package zelda;

import static org.junit.Assert.*;
import org.junit.*;

import java.awt.Toolkit;
import java.awt.Cursor;

public class MainTest
{
	private Main main;
	private Cursor cursor;
	
	@Before
	public void setUp()
	{
		main = new Main();
	}
	
	@Test
	public void cursorShouldBeTransparent()
	{
		cursor = main.getContentPane().getCursor();
		assertTrue(Main.blankCursor.equals(cursor));
	}
}