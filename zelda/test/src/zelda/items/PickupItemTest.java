package zelda.items;

import static org.junit.Assert.*;
import org.junit.*;

import zelda.collision.Weapon;
import zelda.engine.Game;
import zelda.link.Link;

public class PickupItemTest
{
	private Link link;
	
	private ArrowDrop arrow;
	private BombDrop  bomb;
	private Rupee     rupee;
	
	private int oldArrowcount;
	private int newArrowcount;
	private int oldBombcount;
	private int newBombcount;
	private int oldRupeecount;
	private int newRupeecount;
	
	@Before
	public void setUp() throws InterruptedException
	{
		int linkX = 10;
		int linkY = 10;
		int itemX = 20;
		int itemY = 20;
		
		Game game = new Game();
		link = game.getLink();
		
		oldBombcount  = link.getBombcount();
		oldArrowcount = link.getArrowcount();
		oldRupeecount = link.getRupeecount();
		
		arrow = new ArrowDrop(game, itemX, itemY);
		bomb  = new BombDrop(game, itemX, itemY);
		rupee = new Rupee(game, itemX, itemY);
		Thread.sleep(PickupItem.PICKUP_DELAY);
	}
	
	@Test
	public void shouldIncrementLinkItemCountWhenHitWithSword()
	{
		arrow.hitBy(Weapon.SWORD);
		bomb.hitBy(Weapon.SWORD);
		rupee.hitBy(Weapon.SWORD);
		assertFalse(arrow.isAlive());
		assertFalse(bomb.isAlive());
		assertFalse(rupee.isAlive());
		
		newArrowcount = link.getArrowcount();
		newBombcount  = link.getBombcount();
		newRupeecount = link.getRupeecount();
		assertTrue(newArrowcount == oldArrowcount + 10);
		assertTrue(newBombcount  == oldBombcount  +  1);
		assertTrue(newRupeecount == oldRupeecount +  5);
	}
	
	@Test
	public void shouldIncrementLinkItemCountWhenCollidesWithLink()
	{
		arrow.collision(link);
		bomb.collision(link);
		rupee.collision(link);
		assertFalse(arrow.isAlive());
		assertFalse(bomb.isAlive());
		assertFalse(rupee.isAlive());
		
		newArrowcount = link.getArrowcount();
		newBombcount  = link.getBombcount();
		newRupeecount = link.getRupeecount();
		assertTrue(newArrowcount == oldArrowcount + 10);
		assertTrue(newBombcount  == oldBombcount  +  1);
		assertTrue(newRupeecount == oldRupeecount +  5);
	}
}