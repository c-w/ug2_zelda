package zelda.enemy;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.junit.*;

import java.util.ArrayList;

import zelda.engine.Game;
import zelda.karacter.Direction;

public class RandomBehaviorTest
{
	private ArrayList<Direction> collisionDirections;
	private Direction initialDirection;
	private Direction oldDirection;
	private Direction newDirection;
	
	private WhiteSoldier spySoldier;
	private RandomBehavior spyBehavior;
	
	@Before
	public void setUp()
	{	
		Game game = mock(Game.class);
		int x = 10;
		int y = 10;
		initialDirection = Direction.LEFT;
		
		WhiteSoldier whiteSoldier = new WhiteSoldier(game, x, y, initialDirection);
		spySoldier = spy(whiteSoldier);
		
		RandomBehavior randomBehavior = new RandomBehavior(spySoldier);
		spyBehavior = spy(randomBehavior);
	}
	
	@Test
	public void shouldStartInRandomBehavior()
	{
		assertTrue(spySoldier.getBehavior() instanceof RandomBehavior);
	}
	
	@Test
	public void shouldSwitchToAttackBehaviorIfSoldierCanSeeLink()
	{
		doReturn(true).when(spySoldier).canSeeLink();
		
		spyBehavior.behave();
		
		assertTrue(spySoldier.getBehavior() instanceof AttackBehavior);
	}
	
	@Test
	public void shouldSwitchDirectionIfSoldierCollides()
	{
		collisionDirections = new ArrayList<Direction>();
		collisionDirections.add(initialDirection);
		collisionDirections.add(Direction.DOWN);
		doReturn(false).when(spySoldier).canSeeLink();
		doReturn(true).when(spySoldier).collidesOnMoveCurrentDirection(anyInt());
		doReturn(collisionDirections).when(spySoldier).collidesOnMoveAnyDirection(anyInt());
		
		oldDirection = spySoldier.getDirection();
		assertEquals(oldDirection, initialDirection);
		
		spyBehavior.behave();
		
		newDirection = spySoldier.getDirection();
		assertFalse(newDirection.equals(oldDirection));
	}
	
	@Test
	public void shouldSwitchDirectionAfterSomeTime()
	{
		collisionDirections = new ArrayList<Direction>();
		doReturn(false).when(spySoldier).canSeeLink();
		doReturn(false).when(spySoldier).collidesOnMoveCurrentDirection(anyInt());
		doReturn(true).when(spyBehavior).tooLongSinceLastInput();
		doReturn(collisionDirections).when(spySoldier).collidesOnMoveAnyDirection(anyInt());
		
		oldDirection = spySoldier.getDirection();
		assertEquals(oldDirection, initialDirection);
		
		spyBehavior.behave();
		
		newDirection = spySoldier.getDirection();
		assertFalse(newDirection.equals(oldDirection));
	}
}