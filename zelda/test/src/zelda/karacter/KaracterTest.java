package zelda.karacter;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.junit.*;

import java.util.ArrayList;

import zelda.engine.Game;

public class KaracterTest
{
	private KaracterTester spyKaracter;
	private int x;
	private int y;
	private int move;
	private Direction direction;
	private ArrayList<Direction> collisionDirections    = new ArrayList<Direction>();
	private ArrayList<Direction> nonCollisionDirections = new ArrayList<Direction>();
	
	@Before
	public void setUp()
	{
		Game game = mock(Game.class);
		x = 5;
		y = 10;
		move = 5;
		direction = Direction.LEFT;
		int width = 10;
		int height = 20;
		String image = "images/white-soldier.png";
		
		KaracterTester karacter = new KaracterTester(game, x, y, width, height, direction, image);
		spyKaracter = spy(karacter);
		
		collisionDirections.add(direction);
		for (Direction d : Direction.values())
		{
			nonCollisionDirections.add(d);
		}
		nonCollisionDirections.remove(direction);
	}
	
	@Test
	public void shouldSignalCollisionIfWouldMoveOutOfScreen()
	{
		doReturn(true).when(spyKaracter).isOutOfBounds(x-move, y);
		doReturn(false).when(spyKaracter).isCollision(x-move, y);
		assertTrue(spyKaracter.collidesOnMove(direction, move));
		
		doReturn(false).when(spyKaracter).isOutOfBounds(x-move, y);
		doReturn(true).when(spyKaracter).isCollision(x-move, y);
		assertTrue(spyKaracter.collidesOnMove(direction, move));
		
		doReturn(true).when(spyKaracter).isOutOfBounds(x-move, y);
		doReturn(true).when(spyKaracter).isCollision(x-move, y);
		assertTrue(spyKaracter.collidesOnMove(direction, move));
		
		doReturn(false).when(spyKaracter).isOutOfBounds(x-move, y);
		doReturn(false).when(spyKaracter).isCollision(x-move, y);
		assertFalse(spyKaracter.collidesOnMove(direction, move));
	}
	
	@Test
	public void oneCollisionShouldTriggerAllCollidesOnMethods()
	{
		doReturn(true).when(spyKaracter).collidesOnMove(direction, move);
		for (Direction d : nonCollisionDirections)
		{
			doReturn(false).when(spyKaracter).collidesOnMove(d, move);
		}
		
		assertTrue(spyKaracter.collidesOnMoveCurrentDirection(move));
		assertEquals(spyKaracter.collidesOnMoveAnyDirection(move), collisionDirections);
	}
	
	/**
	 * Dummy class to be able to instanciate abstract Karacter class for testing purposes.
	 * 
	 */
	public class KaracterTester extends Karacter
	{
		public KaracterTester(Game game, int x, int y, int width, int height, Direction direction, String image)
		{
			super(game, x, y, width, height, direction, image);
		}
	}
}