package zelda.engine;

import java.io.Serializable;
import zelda.link.Link;

/**
 * Save Link's current number of rupees/hearts/arrows/bombs and scene.
 * 
 * @author maartenhus
 * @author c-w
 */
public class SaveData implements Serializable
{
	private int health;
	private int rupeecount;
	private int arrowcount;
	private int bombcount;

	private String sceneName;

	public SaveData(Link link, Scene scene)
	{
		health     = link.getHealth();
		rupeecount = link.getRupeecount();
		arrowcount = link.getArrowcount();
		bombcount  = link.getBombcount();
		
		sceneName  = scene.getName();
	}
	
	public int getHealth()
	{
		return health;
	}

	public int getRupeecount()
	{
		return rupeecount;
	}
	
	public int getArrowcount()
	{
		return arrowcount;
	}
	
	public int getBombcount()
	{
		return bombcount;
	}
	
	public String getSceneName()
	{
		return sceneName;
	}
}
