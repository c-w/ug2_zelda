package zelda.engine;

/**
 * This class represents overlaid GUI-objects.
 * A GUI-object is always at same position on screen and overlays but doesn't interfere with other objects.
 * 
 * @author c-w
 */
public abstract class GuiObject extends GObject
{
	public GuiObject(Game game, int x, int y, int width, int height, String image)
	{
		super(game, x, y, width, height, image);
		
		z = 2;
		
		screenAdjust = false;
		checkcollision = false;
		liquid = true;
	}
}
