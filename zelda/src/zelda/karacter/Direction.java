package zelda.karacter;

/**
 * These are the directions link or soldiers can face.
 *
 * @author maartenhus
 */
public enum Direction
{
	LEFT, RIGHT, UP, DOWN
}
