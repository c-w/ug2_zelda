package zelda.karacter;

import java.util.ArrayList;
import zelda.engine.GObject;
import zelda.engine.Game;

/**
 * A GObject that has a state and a direction.
 *
 * @author maartenhus
 * @author c-w
 */
public abstract class Karacter extends GObject
{
	protected Direction direction;
	protected KaracterState state;
	protected int health = 1;
	
	public Karacter(Game game, int x, int y, int width, int height, Direction direction, String image)
	{
		super(game, x, y, width, height, image);
		this.direction = direction;
	}
	
	/**
	 * Checks whether the karacter will collide or go out of the screen
	 * if it moves some steps into some direction.
	 * 
	 * @param direction - direction in which to check for collision
	 * @param move - amount of free space there has to be in $direction
	 */ 
	public boolean collidesOnMove(Direction direction, int move)
	{
		int newX = getX();
		int newY = getY();
		
		if (direction == Direction.LEFT)
		{
			newX -= move;
		}
		else if (direction == Direction.RIGHT)
		{
				newX += move;
		}
		else if (direction == Direction.UP)
		{
			newY -= move;
		}
		else if (direction == Direction.DOWN)
		{
			newY += move;
		}
		
		return isCollision(newX, newY) || isOutOfBounds(newX, newY);
	}
	
	/**
	 * Checks whether the karakter will collide or go out of the screen
	 * if it moves some steps into any direction.
	 * 
	 * @param move - amount of free space there has to be in $direction
	 * @return ArrayList of directions where there is a collision (empty ArrayList = no collisions)
	 */
	public ArrayList<Direction> collidesOnMoveAnyDirection(int move)
	{
		ArrayList<Direction> collisions = new ArrayList<Direction>();
		
		if (collidesOnMove(Direction.LEFT,  move)) { collisions.add(Direction.LEFT);  }
		if (collidesOnMove(Direction.RIGHT, move)) { collisions.add(Direction.RIGHT); }
		if (collidesOnMove(Direction.UP,	move)) { collisions.add(Direction.UP);    }
		if (collidesOnMove(Direction.DOWN,  move)) { collisions.add(Direction.DOWN);  }
		
		return collisions;
	}
	
	/**
	 * Checks whether the karakter will collide or go out of the screen
	 * if it moves some steps into its current direction.
	 * 
	 * @param move - amount of free space there has to be in karacter's current direction
	 */
	public boolean collidesOnMoveCurrentDirection(int move)
	{
		return collidesOnMove(direction, move);
	}
	
	public Direction getDirection()
	{
		return direction;
	}
	
	public void setDirection(Direction direction)
	{
		this.direction = direction;
	}
	
	public KaracterState getState()
	{
		return state;
	}
	
	public String getStateString()
	{
		return state.toString();
	}
	
	public void setState(KaracterState state)
	{
		this.state = state;
	}
	
	public int getHealth()
	{
		return health;
	}
	
	public void setHealth(int health)
	{
		this.health = health;
	}
}
