package zelda.collision;

/**
 * Can something can get hit by a weapon?
 * 
 * @author maartenhus
 */
public interface Hittable
{
	/**
	 * Defines what GObject should do if it his by some weapon.
	 * 
	 * @param weapon - represents the Weapon the object was hit by.
	 */
	public void hitBy(Weapon weapon);
}
