package zelda.enemy;

import zelda.engine.Game;
import zelda.karacter.Direction;

/**
 * A White soldier - randomly walks around the map.
 *
 * @author maartenhus
 * @author c-w
 */
public class WhiteSoldier extends Soldier
{
	public WhiteSoldier(Game game, int x, int y, Direction direction)
	{
		super(game, x, y, direction, "images/white-soldier.png");
		behavior = new RandomBehavior(this);
	}
}
