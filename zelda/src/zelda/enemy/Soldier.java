package zelda.enemy;

import java.awt.Polygon;
import java.awt.geom.Area;
import java.awt.Rectangle;
import zelda.engine.Game;
import zelda.karacter.Direction;
import zelda.karacter.Karacter;
import zelda.collision.Hittable;
import zelda.collision.Weapon;

/**
 * Superclass for all enemies.
 * An enemy is a Karacter that can be hit and killed (default health: 6).
 * Soldiers behave according to some behavior (AttackBehavior, RandomBehavior, PatrolBehavior).
 * Soldiers may transition between behavioral states.
 * 
 * @author maartenhus
 * @author c-w
 */
public abstract class Soldier extends Karacter implements Hittable
{
	public final static int WALK_SPEED = 2;
	
	protected Behavior behavior;
	private Polygon eyeView;
	protected long inputInterval = 50;
	protected long lastInput = System.currentTimeMillis();
	protected long lastHit   = System.currentTimeMillis();

	public Soldier(Game game, int x, int y, Direction direction, String image)
	{
		super(game, x, y, 10, 20, direction, image);

		spriteLoc.put("Stand right",	new Rectangle(0, 0, 27, 27));
		spriteLoc.put("Walk right 1",	new Rectangle(30, 0, 32, 27));
		spriteLoc.put("Walk right 2",	new Rectangle(64, 0, 29, 27));

		spriteLoc.put("Stand left",		new Rectangle(0, 30, 27, 27));
		spriteLoc.put("Walk left 1",	new Rectangle(30, 30, 32, 27));
		spriteLoc.put("Walk left 2",	new Rectangle(64, 30, 29, 27));

		spriteLoc.put("Stand up",		new Rectangle(0, 60, 22, 24));
		spriteLoc.put("Walk up 1",		new Rectangle(30, 60, 22, 24));
		spriteLoc.put("Walk up 2",		new Rectangle(60, 60, 22, 25));

		spriteLoc.put("Stand down",		new Rectangle(0, 90, 22, 33));
		spriteLoc.put("Walk down 1",	new Rectangle(30, 90, 22, 34));
		spriteLoc.put("Walk down 2",	new Rectangle(60, 90, 22, 38));
		spriteLoc.put("Walk down 3",	new Rectangle(0, 125, 22, 35));

		spriteLoc.put("hit right",		new Rectangle(0, 160, 27, 27));
		spriteLoc.put("hit left",		new Rectangle(0, 192, 27, 27));
		spriteLoc.put("hit down",		new Rectangle(0, 256, 22, 33));
		spriteLoc.put("hit up",			new Rectangle(0, 224, 22, 24));

		sprite.setSprite(spriteLoc.get("Stand right"));

		health = 6;

		state = new WalkState(this);
	}

	@Override
	public void preAnimation()
	{
		state.handleAnimation();
	}

	@Override
	public void doInLoop()
	{
		if (System.currentTimeMillis() > lastInput + inputInterval)
		{
			state.handleInput();
			behavior.behave();
			lastInput = System.currentTimeMillis();
		}
	}
	
	/**
	 * 
	 * @return boolean: can this instance of can Soldier see Link?
	 */
	public boolean canSeeLink()
	{
		getGame().getScene().removeEyeView(eyeView);
		
		switch (getDirection())
		{
			case UP:
				int[] evxposup = {getX(), getX() - 30, getX() - 20, getX() + 35, getX() + 45, getX() + 15};
				int[] evyposup = {getY(), getY() - 40, getY() - 50, getY() - 50, getY() - 40, getY()};
				eyeView = new Polygon(evxposup, evyposup, evxposup.length);
				getGame().getScene().addEyeView(eyeView);
				break;
			
			case DOWN:
				int[] evxposdown = {getX(), getX() - 30, getX() - 20, getX() + 35, getX() + 45, getX() + 15};
				int[] evyposdown = {getY() + getHeight(), getY() + getHeight() + 40, getY() + getHeight() + 50, getY() + getHeight() + 50, getY() + getHeight() + 40, getY() + getHeight()};
				eyeView = new Polygon(evxposdown, evyposdown, evxposdown.length);
				getGame().getScene().addEyeView(eyeView);
				break;
			
			case LEFT:
				int[] evxposleft = {getX(), getX() - 40, getX() - 50, getX() - 50, getX() - 40, getX()};
				int[] evyposleft = {getY() + 20 , getY() + 50, getY() + 40, getY() - 15, getY() - 25, getY() + 5};
				eyeView = new Polygon(evxposleft, evyposleft, evxposleft.length);
				getGame().getScene().addEyeView(eyeView);
				break;
			
			case RIGHT:
				int[] evxposright = {getX() + getWidth(), getX() + getWidth() + 40, getX() + getWidth() + 50, getX() + getWidth() + 50, getX() + getWidth() + 40, getX() + getWidth()};
				int[] evyposright = {getY() + 20, getY() + 50, getY() + 40, getY() - 15, getY() - 25, getY() + 5};
				eyeView = new Polygon(evxposright, evyposright, evxposright.length);
				getGame().getScene().addEyeView(eyeView);
				break;
		}
		
		final Area area = new Area();
		area.add(new Area(eyeView));
		area.intersect(new Area(getGame().getLink().getRectangle()));
		
		return !area.isEmpty();
	}
	
	/**
	 * Default being-hit behavior: bombs kill, sword deals 3 damage, arrows deal 2 damage.
	 * If Soldier is killed it may drop a random item.
	 * 
	 */
	public void hitBy(Weapon weapon)
	{
		if (health >= 1)
		{
			game.playFx("sounds/enemyHit.mp3");
		}
		
		switch(weapon)
		{
			case SWORD:
				if (health > 0 && System.currentTimeMillis() > lastHit + 800)
				{
					lastHit = System.currentTimeMillis();
					health -= 3;
					setState(new TransState(this, game.getLink().getDirection()));
					setBehavior(new AttackBehavior(this));
				}
				break;
			
			case BOMB:
				health = 0;
				break;
			
			case ARROW:
				if (health > 0 && System.currentTimeMillis() > lastHit + 800)
				{
					lastHit = System.currentTimeMillis();
					health -= 2;
					setBehavior(new AttackBehavior(this));
				}
				break;
		}
		
		if(health <= 0)
		{
			alive = false;
			game.playFx("sounds/enemyDie.mp3");
			randomGoodie();
		}
	}
	
	public Behavior getBehavior()
	{
		return behavior;
	}

	public void setBehavior(Behavior behavior)
	{
		this.behavior = behavior;
	}
}
