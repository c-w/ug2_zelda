package zelda.enemy;

/**
 * Enemies have different behaviors, some patrol an area. Some may stand still
 * and others randomly move around.
 *
 * @author maartenhus
 */
public abstract class Behavior
{
	public abstract void behave();
}
