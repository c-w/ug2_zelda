package zelda.enemy;

import zelda.karacter.Direction;

/**
 * If Karacter can see Link, change to AttackBehavior.
 * Otherwise, keep moving in current direction for some time and the come back.
 * 
 * @author maartenhus
 * @author c-w
 */
public class PatrolBehavior extends Behavior
{
	private BlueSoldier soldier;
	private int ticks = 0;
	private int max;
	private int step = 1;
	
	protected Behavior behavior;

	public PatrolBehavior(BlueSoldier soldier, int ticks)
	{
		this.soldier = soldier;
		this.max = ticks;
		move();
	}

	public void behave()
	{
		if(soldier.canSeeLink())
		{
			soldier.setBehavior(new AttackBehavior(soldier));
			return;
		}
		
		if (soldier.getStateString().equals("WalkState"))
		{
			ticks += step;

			if (ticks > max)
			{
				move();
				ticks = -1;
			}
		}
	}

	private void move()
	{
		switch (soldier.getDirection())
		{
			case UP:
				soldier.setDirection(Direction.DOWN);
				break;

			case DOWN:
				soldier.setDirection(Direction.UP);
				break;

			case LEFT:
				soldier.setDirection(Direction.RIGHT);
				
				break;

			case RIGHT:
				soldier.setDirection(Direction.LEFT);
				break;
		}
	}
}
