package zelda.enemy;

import zelda.engine.Game;
import zelda.karacter.Direction;

/**
 * A Blue soldier - patrols a given path on the map.
 *
 * @author maartenhus
 * @author c-w
 */
public class BlueSoldier extends Soldier
{
	public BlueSoldier(Game game, int x, int y, Direction direction, int ticks)
	{
		super(game, x, y, direction, "images/blue-soldier.png");
		behavior = new PatrolBehavior(this, ticks);
	}
}
