package zelda.enemy;

import java.util.ArrayList;
import zelda.karacter.Direction;

/**
 * If Karacter can see Link, change to AttackBehavior.
 * Otherwise, keep moving in current direction if the way is free.
 * Randomly change into a new direction every now and then.
 * 
 * @author Christiaan
 * @author c-w
 */
public class RandomBehavior extends Behavior 
{
	private WhiteSoldier soldier;
	
	private int move;
	private ArrayList<Direction> collisions;

	private long inputInterval = 5000;
	private long lastInput = System.currentTimeMillis();

	public RandomBehavior(WhiteSoldier soldier)
	{
		this.soldier = soldier;
		this.move    = Soldier.WALK_SPEED << 3;
	}
	
	public void behave()
	{
		if(soldier.canSeeLink())
		{
			soldier.setBehavior(new AttackBehavior(soldier));
			return;
		}
		
		if (soldier.collidesOnMoveCurrentDirection(move) || tooLongSinceLastInput())
		{
			collisions = soldier.collidesOnMoveAnyDirection(move);
			
			ArrayList<Direction> possibleDirections = new ArrayList<Direction>();
			possibleDirections.add(Direction.DOWN);
			possibleDirections.add(Direction.UP);
			possibleDirections.add(Direction.RIGHT);
			possibleDirections.add(Direction.LEFT);
			
			for (Direction collisionDirection : collisions)
			{
				possibleDirections.remove(collisionDirection);
			}
			
			possibleDirections.remove(soldier.getDirection());
			
			try // for some reason, possibleDirections is empty sometimes
			{
				int r = (int) (Math.random()*possibleDirections.size());
				
				soldier.setDirection(possibleDirections.get(r));
				
				lastInput = System.currentTimeMillis();
			}
			catch(IndexOutOfBoundsException e)
			{
			}
		}
	}
	
	public boolean tooLongSinceLastInput()
	{
		return System.currentTimeMillis() > lastInput + inputInterval;
	}
}
