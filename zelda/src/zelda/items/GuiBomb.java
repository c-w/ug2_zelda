package zelda.items;

import java.awt.Rectangle;
import zelda.engine.Game;
import zelda.engine.GuiObject;

/**
 * GUI-element showing Link's current number of bombs.
 * 
 * @author c-w
 */

public class GuiBomb extends GuiObject
{
	private final static String[] bombAnimation = {"bomb"};

	public GuiBomb(Game game, int x, int y)
	{
		super(game, x, y, 12, 12, "images/guibomb.png");
		spriteLoc.put("bomb",new Rectangle(0, 0, 12, 12));

		setAnimation(bombAnimation);
	}
}
