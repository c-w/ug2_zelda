package zelda.items;

import java.awt.Rectangle;
import zelda.engine.Game;

/**
 *
 * @author Tom
 * @author c-w
 */

public class Heart extends PickupItem
{
	private final static String[] heartAnimation = {"heart"};

	public Heart (Game game, int x, int y)
	{
		super(game, x, y, 11, 10, "images/heart1.png");
		spriteLoc.put("heart",new Rectangle(0, 0, 11, 10));
	
		sprite.setSprite(spriteLoc.get("heart"));
		setAnimation(heartAnimation);
	}
}
