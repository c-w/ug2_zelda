package zelda.items;

import java.awt.Rectangle;
import zelda.engine.Game;

/**
 * This class represents the a dropped (i.e. not shot) arrow
 *
 * @author c-w
 */
public class ArrowDrop extends PickupItem
{
	private final static String[] arrowdropAnimation = {"arrowdrop1"};
	
	public ArrowDrop (Game game, int x, int y)
	{
		super(game, x, y, 5, 15, "images/arrowdrop.png");
		spriteLoc.put("arrowdrop1", new Rectangle(0, 0, 5, 15));
		
		sprite.setSprite(spriteLoc.get("arrowdrop1"));
		
		setAnimationInterval(100);
		setAnimation(arrowdropAnimation);
	}
}
