package zelda.items;

import java.awt.Rectangle;
import zelda.engine.Game;
import zelda.engine.GuiObject;

/**
 * GUI-element showing Link's current number of rupees.
 * 
 * @author Bas Harteveld
 * @author c-w
 */

public class GuiRupee extends GuiObject
{
	private final static String[] rupeeAnimation = {"rupee"};

	public GuiRupee(Game game, int x, int y)
	{
		super(game, x, y, 11, 10, "images/guirupee.png");
		spriteLoc.put("rupee",new Rectangle(0, 0, 11, 10));

		setAnimation(rupeeAnimation);
	}
}
