package zelda.items;

import zelda.collision.Hittable;
import zelda.collision.Weapon;
import zelda.engine.GObject;
import zelda.engine.Game;
import zelda.link.Link;

/**
 * A PickupItem is something that can be picked-up by Link.
 * 
 * @author c-w
 */
public abstract class PickupItem extends GObject implements Hittable
{
	public final static long PICKUP_DELAY = 500;
	
	public PickupItem(Game game, int x, int y, int width, int height, String image)
	{
		super(game, x, y, width, height, image);
		
		liquid = true;
		z = 1;
	}
	
	/** 
	 * Makes item collectable by hitting it with sword.
	 * Implemented with slight delay to prevent immediate post-drop pickup.
	 * 
	 * @param weapon - weapon that hits the object
	 */
	public void hitBy(Weapon weapon)
	{
		if (weapon == Weapon.SWORD)
		{
			if (System.currentTimeMillis() > this.timeCreated + PICKUP_DELAY)
			{
				game.getLink().pickup(this);
				alive = false;
			}
		}
	}
	
	/**
	* Makes item collectable by link walking over it.
	* 
	*/
	@Override
	public void collision(GObject obj)
	{
		if (obj instanceof Link)
		{
			game.getLink().pickup(this);
			alive = false;
		}
	}
}
