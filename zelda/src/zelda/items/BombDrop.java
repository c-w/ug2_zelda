package zelda.items;

import java.awt.Rectangle;
import zelda.engine.Game;

/**
 * This class represents a droped (i.e. unset!) bomb
 * 
 * @author c-w
 */
public class BombDrop extends PickupItem
{
	private final static String[] bombdropAnimation = {"bombdrop1"};
	
	public BombDrop (Game game, int x, int y)
	{
		super(game, x, y, 13, 16, "images/bombdrop.png");
		spriteLoc.put("bombdrop1", new Rectangle(0, 0, 13, 16));
		
		sprite.setSprite(spriteLoc.get("bombdrop1"));
		
		setAnimationInterval(100);
		setAnimation(bombdropAnimation);
	}
}
