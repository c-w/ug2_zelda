package zelda.items;

import java.awt.Rectangle;
import zelda.engine.Game;
import zelda.engine.GuiObject;

/**
 * GUI-element showing Link's current number of arrows.
 * 
 * @author c-w
 */

public class GuiBow extends GuiObject
{
	private final static String[] bowAnimation = {"bow"};

	public GuiBow(Game game, int x, int y)
	{
		super(game, x, y, 12, 12, "images/guibow.png");
		spriteLoc.put("bow",new Rectangle(0, 0, 12, 12));

		setAnimation(bowAnimation);
	}
}
