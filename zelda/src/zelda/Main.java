package zelda;

import java.awt.Toolkit;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;
import zelda.engine.Game;

/**
 *
 * @author ?
 * @author c-w
 */
public class Main extends JFrame
{
	public final static Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(new BufferedImage(8, 8, BufferedImage.TYPE_INT_ARGB), new Point(0,0), "blank cursor");
	
	private Game game;
	private View view;
	private Controller ctl;
	
	/**
	 * Provides main JFrame functionalities.
	 * 
	 */
	public Main()
	{
		setIgnoreRepaint(true);
		
		game = new Game();
		
		if(game.isDebug())
		{
			setLocationRelativeTo(null);
			setSize(game.getWidth(), game.getHeight());
		}
		else
		{
			removeCursor();
		}
		
		setUndecorated(true);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
		view = new View(game, this);
		ctl = new Controller(game, view, this);
	}
	
	/**
	* Hides cursor by setting it to an empty image.
	*
	*/
	private void removeCursor()
	{	
		this.getContentPane().setCursor(blankCursor);
	}
	
	public static void main(String[] args)
	{
		new Main();
	}
}
