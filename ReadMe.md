## Zelda game for Sofware Engineering Assignment

### Build and run the project
- ```ant run```

### Documentation
- ```ant doc```
- ```ant usage```

### Libraries
- [JLayer](http://www.javazoom.net/javalayer/sources.html)
- [JUnit](http://www.junit.org/)
- [Mockito](http://code.google.com/p/mockito/)
